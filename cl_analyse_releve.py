#!/usr/bin/python3
"""Classe pour gérer la BD et les relevés qui y sont importés"""
import csv
import sqlite3
from datetime import datetime as dt
from os import path, makedirs


class cl_analyse_releve(object):
    """Cré un objet pour analyser le relevé passer à la fonction .executer()\nafin de l'importer dans la BD. """
    def __init__(self, nom_bd, fic_releve):
        self.path_bd = path.join(path.dirname(path.realpath(__file__)), "BaseDonnees")
        self.nom_bd = path.join(self.path_bd, nom_bd)
        print('Nom de la BD utilisé : {}'.format(self.nom_bd))
        self.nom_releve = nom_releve
        self.debug = True
        self.connection = ""
        self.cursor = ""
        self.f_initialiser()

    def f_initialiser(self):
        """Validation que toutes les tables sont créées.\nCréation du PATH de la BD si nécessaire."""
        if not path.exists(self.path_bd):
            print("Path vers {} n'existe pas!".format(self.path_bd))
            makedirs(self.path_bd)
        self.create_table('')
        if self.debug:
            print('Tables validées')

    def f_connect(self):
        """Connection à la BD"""
        self.connection = sqlite3.connect(self.nom_bd)
        self.cursor = self.connection.cursor()

    def f_disconnect(self, commit):
        """Déconnection de la BD"""
        if commit:
            self.connection.commit()
        self.cursor.close()
        self.connection.close()

    def vider_table(self, table):
        """Destruction et recréation de la table passée en paramètre."""
        sql_drop = "DROP TABLE IF EXISTS {}; VACUUM;".format(table)
        self.f_connect()
        self.cursor.executescript(sql_drop)
        self.create_table(table)
        # Laisser le soin a create_table de commiter et fermer la BD
        # self.f_disconnect('commit')
        if table == 'table_debiteurs_temp':
            self.cursor.execute('''INSERT INTO table_debiteurs_temp
                                    SELECT no_debiteur, nom_debiteur, categorie, non_constant
                                    FROM table_debiteurs''')
            self.connection.commit()

  #----------------------------------------------

    def create_table(self, table):
        """Cré la table demandée, laisser à vide pour toutes les créer"""
        lst_sql_create = {}
        # table_cc - no_carte, no_sur_carte, expiration, CVV, institution, compte, nom_affichage, info
        lst_sql_create['table_cc'] = '''CREATE TABLE IF NOT EXISTS table_cc(
            no_carte INTEGER PRIMARY KEY,
            no_sur_carte INTEGER,
            expiration REAL,
            CVV INTEGER,
            institution TEXT,
            compte INTEGER,
            nom_affichage TEXT,
            info TEXT)'''
        # table_cc_transactions - no_releve , date_annee, date_mois, date_jour, debiteur, montant, categorie_forcee
        lst_sql_create['table_cc_transactions'] = '''CREATE TABLE IF NOT EXISTS table_cc_transactions(
            no_releve INTEGER,
            date_annee INTEGER,
            date_mois INTEGER,
            date_jour INTEGER,
            debiteur INTEGER,
            montant REAL,
            categorie_forcee INTEGER,
            FOREIGN KEY (no_releve) REFERENCES table_releves (no_releve),
            FOREIGN KEY (debiteur) REFERENCES table_debiteurs (no_debiteur))'''
        # table_cc_transactions_temp - no_releve , date_annee, date_mois, date_jour, debiteur, montant
        lst_sql_create['table_cc_transactions_temp'] = '''CREATE TABLE IF NOT EXISTS table_cc_transactions_temp(
            no_releve INTEGER,
            date_annee INTEGER,
            date_mois INTEGER,
            date_jour INTEGER,
            debiteur INTEGER,
            montant REAL,
            FOREIGN KEY (debiteur) REFERENCES table_debiteurs_temp (no_debiteur))'''
        # table_debiteurs - nom_debiteur, categorie, non_constant
        lst_sql_create['table_debiteurs'] = '''CREATE TABLE IF NOT EXISTS table_debiteurs(
            no_debiteur INTEGER PRIMARY KEY,
            nom_debiteur TEXT,
            categorie INTEGER,
            non_constant INTEGER)'''
        # table_debiteurs - nom_debiteur, categorie, non_constant
        lst_sql_create['table_debiteurs_temp'] = '''CREATE TABLE IF NOT EXISTS table_debiteurs_temp(
            no_debiteur INTEGER PRIMARY KEY,
            nom_debiteur TEXT,
            categorie INTEGER,
            non_constant INTEGER)'''
        # table_releves - no_releve, no_carte, nom_fichier, nom_releve, date_deb, date_fin, note
        lst_sql_create['table_releves'] = '''CREATE TABLE IF NOT EXISTS table_releves(
            no_releve INTEGER PRIMARY KEY,
            no_carte INTEGER,
            nom_fichier TEXT,
            nom_releve TEXT,
            date_deb TEXT,
            date_fin TEXT,
            note TEXT,
            FOREIGN KEY (no_carte) REFERENCES table_cc (no_carte))'''
        # table_categories - nom_categorie, note
        lst_sql_create['table_categories'] = '''CREATE TABLE IF NOT EXISTS table_categories(
            no_categorie INTEGER PRIMARY KEY,
            nom_categorie TEXT,
            categorie_parent INTEGER,
            note TEXT,
            FOREIGN KEY (categorie_parent) REFERENCES table_categories (no_categorie))'''

        self.f_connect()

        if table == '':
            for nom_table_a_creer in lst_sql_create:
                self.cursor.execute(lst_sql_create[nom_table_a_creer])
        else:
            self.cursor.execute(lst_sql_create[table])

        self.f_disconnect('commit')
  #----------------------------------------------

    def ajout_transaction(self, donnee_a_inserer, type_donnee):
        """Ajoute une transaction à la table des transactions (réel ou temp) sans validation."""

        if type_donnee == 'temp':
            suffix = '_temp'
            ref_debiteur = 'nom'
        else:
            suffix = ''
            ref_debiteur = 'no'
        sql_insertion = '''INSERT INTO table_cc_transactions{}(no_releve, date_annee, date_mois, date_jour, debiteur, montant)
                            VALUES (?, ?, ?, ?, (SELECT no_debiteur FROM table_debiteurs{} WHERE {}_debiteur=?), ?)'''.format(suffix, suffix, ref_debiteur)

        self.cursor.execute(sql_insertion, donnee_a_inserer)
        return 1

    def ajout_debiteur(self, donnee_a_inserer, type_donnee):
        """Ajoute le débiteur d'une transaction (réel ou temp) s'il n'existe pas déjà"""
        doublon = 0
        if type_donnee == 'temp':
            suffix = '_temp'
        else:
            suffix = ''
        sql_selection = ("SELECT no_debiteur FROM table_debiteurs{} WHERE nom_debiteur=?".format(suffix))
        sql_insertion = ("INSERT INTO table_debiteurs{}(nom_debiteur) VALUES (?)".format(suffix))

    # -------- DEBUG
    #     print(type_donnee)
    #     print("sql_selection == {}".format(sql_selection))
    #     print("donnee_a_inserer == {}".format(donnee_a_inserer))
    #     print("sql_insertion == {}".format(sql_insertion))
    # -------- DEBUG

        self.cursor.execute(sql_selection, donnee_a_inserer)
        for chaque_doublon in self.cursor.fetchall():
            if self.debug:
                print(suffix, chaque_doublon)
            doublon += 1
        if doublon == 0:
            self.cursor.execute(sql_insertion, donnee_a_inserer)
            return 1
        else:
            doublon = 0
            return 0

    def f_ajout_releve(self, fic_releve):
        """Retourne si un relevé a déjà été ajouté (return 0)
        ou s'il peut être ajouté (return nouveau numéro du relevé).
        Si le relevé n'est pas présent, il est ajouté à la BD."""
        sql_select = "SELECT no_releve FROM table_releves WHERE nom_fichier = ?"
        self.f_connect()
        path_fic_releve = (path.split(fic_releve))
        self.cursor.execute(sql_select, (path_fic_releve[1],))
        for no_releve in self.cursor.fetchall():
            self.f_disconnect('')
            return 0

        sql_insert = "INSERT INTO table_releves(nom_fichier) VALUES(?)"
        self.cursor.execute(sql_insert, (path_fic_releve[1],))
        self.connection.commit()
        self.cursor.execute(sql_select, (path_fic_releve[1],))
        for no_releve in self.cursor.fetchall():
            self.f_disconnect('')
            return no_releve[0]

    #----------------------------------------------
    def enregistre_releve_scotia(self, fic_releve):
        """Importe un relevé de type 'Scotia' dont les lignes ressemblent à
        7/10/2018,"MCDONALD'S #12013  Q04   MONTREAL     QC ",-22.28
        Ces CSV sont ASCII avec une date M/J/AAAA"""
        count_debiteur = 0
        count_transaction = 0
        no_releve = 0
        # if not no_releve:
        #     print("Relevé {} existe déjà!\nValider la table de relevés".format(fic_releve))
        #     return (count_transaction, count_debiteur)

        self.vider_table('table_cc_transactions_temp')
        self.vider_table('table_debiteurs_temp')

        self.f_connect()
        with open(fic_releve, 'r', encoding='latin-1') as csvfile:
#            ligne_transaction = csv.reader(csvfile, delimiter=',', quotechar='"')
            ligne_transaction = csv.reader(csvfile, delimiter=';')
            for date_releve, trans, montant in ligne_transaction:
                count_debiteur = count_debiteur + self.ajout_debiteur((trans, 'temp'))

                an, mois, jour = dt.strptime(date_releve, '%m/%d/%Y').strftime('%Y %m %d').split()
                self.ajout_transaction((no_releve, an, mois, jour, trans, montant), 'temp')

        self.f_disconnect('commit')
        count_transaction = self.valider_releve(no_releve)
        no_releve = self.f_ajout_releve(fic_releve)

        return (count_transaction, count_debiteur)

    #----------------------------------------------
    def enregistre_releve_desjardins(self, fic_releve):
        """Importe un relevé de type 'Desjardins' dont les lignes ressemblent à
        "Bassin-De-Chambly ","046486","EOP","2018/07/06",00001,"Dépôt - Virement Interac","","",1050.00,"","","","",8950.23
        Ces CSV sont Latin-1 avec une date AAAA/MM/JJ"""
        count_debiteur = 0
        count_transaction = 0
        no_releve = self.f_ajout_releve(fic_releve)
        if not no_releve:
            print("Relevé {} existe déjà!\nValider la table de relevés".format(fic_releve))
            return (count_transaction, count_debiteur)

        self.vider_table('table_cc_transactions_temp')

        self.f_connect()
        with open(fic_releve, 'r', encoding='latin-1') as csvfile:
            ligne_transaction = csv.reader(csvfile, delimiter=',', quotechar='"')
            for nom_caisse, no_compte, type_compte, date_releve, no_operation, nom_transaction, vide1, retrait, depot, vide2, vide3, vide4, vide5, balance in ligne_transaction:
                if self.debug:
                    print("Transaction a insérer:\n{}".format((nom_caisse, no_compte, type_compte, date_releve, no_operation, nom_transaction, vide1, retrait, depot, vide2, vide3, vide4, vide5, balance)))

                count_debiteur = count_debiteur + self.ajout_debiteur((nom_transaction,))
                an, mois, jour = dt.strptime(date_releve, '%Y/%m/%d').strftime('%Y %m %d').split()
                montant = float('0'+depot) - float('0'+retrait)
                self.ajout_transaction((no_releve, an, mois, jour, nom_transaction, montant), 'transaction_temp')

            count_transaction = self.valider_releve(no_releve)
        self.f_disconnect('commit')
        return (count_transaction, count_debiteur)

    #----------------------------------------------
    def valider_releve(self, source_releve):
        """Fonction pour valider si la même transaction, provenanat du même relevé a déjà été importée."""
        doublons = 0
        count_transaction = 0

        self.cursor.execute("SELECT DISTINCT date_annee, date_mois, date_jour FROM table_cc_transactions_temp")

        connection2 = sqlite3.connect(self.nom_bd)
        c2 = connection2.cursor()
        for an, mois, jour in self.cursor.fetchall():
            c2.execute("SELECT count(*) FROM table_cc_transactions WHERE no_releve=? AND date_annee=? AND date_mois=? AND date_jour=?", (source_releve, an, mois, jour))
            for count_trouve in c2.fetchall():
                doublons = doublons + int(count_trouve[0])

        c2.close()
        connection2.close()

        if doublons == 0:
            print('Pas de doublon')
            self.cursor.execute("SELECT date_annee, date_mois, date_jour, debiteur, montant FROM table_cc_transactions_temp")
            for an, mois, jour, debiteur, montant in self.cursor.fetchall():
                count_transaction = count_transaction + self.ajout_transaction((source_releve, an, mois, jour, debiteur, montant), 'transaction')
        else:
            print('doublon trouvé {}'.format(doublons))

        return count_transaction

    #----------------------------------------------
    def table_export(self, table):
        """Fonction pour exporter les tables sans validation.
        Produit un fichier .dump au format CSV dans le répertoire de l'application.
        Le nom sera nom_table.dump"""
        if table == 'table_cc':
            nom_rangees = ['no_carte', 'no_sur_carte', 'expiration', 'CVV', 'institution', 'compte', 'nom_affichage', 'info']
            sql_selection = "SELECT no_carte, no_sur_carte, expiration, CVV, institution, compte, nom_affichage, info FROM table_cc"
        elif table == 'table_debiteurs':
            nom_rangees = ['no_debiteur', 'nom_debiteur', 'categorie', 'non_constant']
            sql_selection = "SELECT no_debiteur, nom_debiteur, categorie, non_constant FROM table_debiteurs"
        elif table == 'table_releves':
            nom_rangees = ['no_releve', 'nom_releve', 'date_deb', 'date_fin', 'note']
            sql_selection = "SELECT no_releve, nom_releve, date_deb, date_fin, note FROM table_releves"
        elif table == 'table_categories':
            nom_rangees = ['no_categorie', 'nom_categorie', 'note']
            sql_selection = "SELECT no_categorie, nom_categorie, note FROM table_categories"
        else:
            return 1

        self.f_connect()
        self.cursor.execute(sql_selection)
        with open(table+'.dump', 'w', newline='', encoding='latin-1') as csvfile:
            ecriture_csv = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            ecriture_csv.writerow(nom_rangees)
            for ligne in self.cursor.fetchall():
                ecriture_csv.writerow(ligne)

        self.f_disconnect('')
        print('Table {} exportée'.format(table))

    #----------------------------------------------
    def table_import(self, table):
        """Fonction pour importer les tables sans validation.
        Requiert un fichier .dump au format CSV dans le répertoire de l'application.
        Le nom doit être nom_table.dump"""
        if table == 'table_cc':
            sql_insertion = "INSERT INTO table_cc(no_carte, no_sur_carte, expiration, CVV, institution, compte, nom_affichage, info) VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
        elif table == 'table_debiteurs':
            sql_insertion = "INSERT INTO table_debiteurs(no_debiteur, nom_debiteur, categorie, non_constant) VALUES (?, ?, ?, ?)"
        elif table == 'table_releves':
            sql_insertion = "INSERT INTO table_releves(no_releve, nom_releve, date_deb, date_fin, note) VALUES (?, ?, ?, ?, ?)"
        elif table == 'table_categories':
            sql_insertion = "INSERT INTO table_categories(no_categorie, nom_categorie, note) VALUES (?, ?, ?)"
        else:
            return 1

        self.vider_table(table)
        self.f_connect()
        with open(table+'.dump', 'r', newline='', encoding='latin-1') as csvfile:
            ligne_transaction = csv.reader(csvfile, delimiter=',', quotechar='"')
            next(ligne_transaction, None) # Sauter le header
            for elements in ligne_transaction:
                self.cursor.execute(sql_insertion, tuple(elements))

        self.f_disconnect('commit')
        print('Table {} importée'.format(table))

    #----------------------------------------------
    # TEMPORAIRE juste pour tout afficher
    def print_transactions(self):
        """TEMPORAIRE juste pour tout afficher"""
        self.f_connect()
        self.cursor.execute('SELECT * FROM table_cc_transactions')
        print(self.cursor.fetchall())
        self.f_disconnect('')
    #----------------------------------------------

    #----------------------------------------------
    def executer(self, nom_releve):
        """Fonction pour importer un relevé.\nSera remplacé pour import par l'application"""
        nouv_transactions, nouv_debiteur = self.enregistre_releve_scotia(nom_releve)
        print("SCOTIA - Transactions insérées -> {}\nSCOTIA - Débiteurs insérés -> {}".format(nouv_transactions, nouv_debiteur))

        # nouv_transactions, nouv_debiteur = self.enregistre_releve_desjardins(nom_releve)
        # print("DESJAR - Transactions insérées -> {}\nDESJAR - Débiteurs insérés -> {}".format(nouv_transactions, nouv_debiteur))

        # export_debiteurs()
