#!/usr/bin/python3
"""Panneau de gestion des catégories appellé par le programme Suivi Budgétaire"""
import sqlite3
from PyQt5.QtWidgets import QMainWindow, QTreeWidgetItem, QApplication
from UI.GestionCategories_ui import Ui_GestionCategories

class FenetreGestionCategorie(QMainWindow):
    """Fenetre servant a gérer les catégories"""
    def __init__(self, nom_bd):
        self.nom_bd = nom_bd
        super(FenetreGestionCategorie, self).__init__()

        # Set up the user interface from Designer.
        self.ui = Ui_GestionCategories()
        self.ui.setupUi(self)

        self.tableau_cbb_categories = {}
        self.lst_c = {}
        self.categ_selectionnee = ""

        self.f_initialisation()

    def f_connect(self):
        """Connection à la BD"""
        self.connection = sqlite3.connect(self.nom_bd)
        self.cursor = self.connection.cursor()

    def f_disconnect(self, commit):
        """Déconnection de la BD"""
        if commit:
            self.connection.commit()
        self.cursor.close()
        self.connection.close()

    def f_initialisation(self):
        """Initialisation de tout ce qui est 'One-Timer'"""
        self.f_load_categories()
        self.ui.trw_lstCategories.setSelectionMode(1) #QAbstractItemView::SingleSelection
        # QAbstractItemView::SelectRows
        # QAbstractItemView::SingleSelection
        self.ui.btn_quitter.clicked.connect(self.close)
        self.ui.btn_editer.clicked.connect(self.f_editer_categorie)
        self.ui.btn_associerCateg.clicked.connect(self.f_associer_categorie)
        self.ui.btn_creer.clicked.connect(self.f_creer)
        self.ui.btn_supprimer.clicked.connect(self.f_supprimer)
        self.ui.cbb_Categories.currentIndexChanged.connect(self.f_cbb_categ_select)
        self.ui.lne_NomCategorie.textChanged.connect(self.f_creer_valide_si_dispo)

    def f_load_categories(self):
        """Ajoute toutes les catégories dans le cbb_categories"""
        self.ui.cbb_Categories.clear()
        self.ui.cbb_Categories.addItem("CHOISIR une catégorie")
        self.ui.lbl_NbrDebiteurAss.setText("Nombre de débiteur associés à cette catégorie : ")
        self.ui.lbl_NomCategorie.setText("")
        self.ui.lne_NomCategorie.setText("")
        self.categ_selectionnee = self.ui.cbb_Categories.currentText()
        self.f_connect()
        # - Remplir cbb_Categories
        self.cursor.execute("SELECT nom_categorie, no_categorie FROM table_categories ORDER BY nom_categorie")
        for element in self.cursor.fetchall():
            self.tableau_cbb_categories[element[0]] = element[1]
            self.ui.cbb_Categories.addItem(element[0])
        # ------------------------
        # - Remplir trw_lstCategories
        self.ui.trw_lstCategories.clear()
        self.ui.trw_lstCategories.setHeaderLabels(("Catégories",))
        self.cursor.execute("SELECT nom_categorie, no_categorie FROM table_categories WHERE categorie_parent IS NULL ORDER BY nom_categorie")
        for element in self.cursor.fetchall():
            qtwi_categorie = QTreeWidgetItem([element[0],])
            self.f_recherche_enfants_categ(qtwi_categorie, element, 1)
            self.ui.trw_lstCategories.addTopLevelItem(qtwi_categorie)
        # ---------------------------
        self.f_disconnect('')

    def f_recherche_enfants_categ(self, qtwi_categorie, t_categorie, niveau_enfant):
        """Cherche les catégories enfants, AKA qui ont la catégorie passé en paramètre comme parent."""
        if niveau_enfant < 10:
            self.lst_c[t_categorie[0]] = self.connection.cursor()
            self.lst_c[t_categorie[0]].execute("SELECT nom_categorie, no_categorie FROM table_categories WHERE categorie_parent = ? ORDER BY nom_categorie", (t_categorie[1],))
            for enfant in self.lst_c[t_categorie[0]].fetchall():
                qtwi_categorie_enfant = QTreeWidgetItem([enfant[0],])
                qtwi_categorie.addChild(qtwi_categorie_enfant)
                self.f_recherche_enfants_categ(qtwi_categorie_enfant, enfant, niveau_enfant+1)
            self.lst_c[t_categorie[0]].close()
        else:
            # écrire dans l'enfant que
            print("Plus de 10 niveau ne sont pas permit")

    def f_cbb_categ_select(self):
        """Modifie les options possibles selon la catégorie choisie dans cbb_Categories."""
        self.categ_selectionnee = self.ui.cbb_Categories.currentText()
        if self.ui.cbb_Categories.currentIndex() > 0:
            if self.ui.lbl_NomCategorie.text() != "" and self.ui.cbb_Categories.currentText() != self.ui.lbl_NomCategorie.text():
                self.ui.btn_associerCateg.setEnabled(True)
            else:
                self.ui.btn_associerCateg.setEnabled(False)

            self.f_connect()
            self.cursor.execute("SELECT count(*) FROM table_debiteurs WHERE categorie=?", (self.tableau_cbb_categories[self.categ_selectionnee], ))
            for nbr in self.cursor.fetchall():
                self.ui.lbl_NbrDebiteurAss.setText("Nombre de débiteur associés à cette catégorie : {}".format(nbr[0]))
                self.ui.btn_supprimer.setEnabled(True) if nbr[0] == 0 else self.ui.btn_supprimer.setEnabled(False)
            self.f_disconnect('')

    def f_editer_categorie(self):
        """Met tout en place pour modifier la catégorie choisie dans cbb_Categories."""
        if self.ui.cbb_Categories.currentIndex() > 0:
            self.ui.lbl_NomCategorie.setText(self.categ_selectionnee)
            self.ui.lne_NomCategorie.setText(self.categ_selectionnee)
            self.ui.btn_associerCateg.setEnabled(False)

    def f_associer_categorie(self):
        """Met la catégorie choisie dans le cbb_Categories comme parent de la catégorie en édition."""
        self.f_connect()
        self.cursor.execute("UPDATE table_categories SET categorie_parent=? WHERE nom_categorie=?", (self.tableau_cbb_categories[self.categ_selectionnee], self.ui.lbl_NomCategorie.text()))
        self.f_disconnect('commit')
        self.f_load_categories()

    def f_enregistrer(self):
        """Enregistre la catégorie en édition."""
        self.f_connect()
        nouveau_nom = self.ui.lne_NomCategorie.text().strip()
        self.cursor.execute("UPDATE table_categories SET nom_categorie = ? WHERE nom_categorie = ?", (nouveau_nom, self.categ_selectionnee))
        self.ui.lbl_NomCategorie.setText("")
        self.ui.lne_NomCategorie.setText("")
        self.f_disconnect('commit')
        self.f_load_categories()
        self.ui.cbb_Categories.setCurrentIndex(self.ui.cbb_Categories.findText(nouveau_nom))

    def f_creer_valide_si_dispo(self):
        """Active les boutons Créer/Enregistrer selon le nom donné."""
        nouveau_nom = self.ui.lne_NomCategorie.text().strip()
        if nouveau_nom != "" and nouveau_nom != self.ui.lbl_NomCategorie.text():
            # Ajouter que la catég ne doit pas être dans self.tableau_cbb_categories
            self.ui.btn_creer.setEnabled(True)
        else:
            self.ui.btn_creer.setEnabled(False)

    def f_creer(self):
        """Créer la nouvelle catégorie."""
        self.f_connect()
        nouveau_nom = self.ui.lne_NomCategorie.text().strip()
        if self.ui.cbb_Categories.currentIndex() > 0:
            self.cursor.execute("INSERT INTO table_categories(nom_categorie) VALUES (?)", (nouveau_nom, ))
        else:
            self.cursor.execute('''INSERT INTO table_categories(nom_categorie, categorie_parent)
                                VALUES (?,(SELECT no_categorie FROM table_categories WHERE nom_categorie=?))''',
                                (nouveau_nom, self.categ_selectionnee))
        self.ui.lbl_NomCategorie.setText("")
        self.ui.lne_NomCategorie.setText("")
        self.f_disconnect('commit')
        self.f_load_categories()
        self.ui.cbb_Categories.setCurrentIndex(self.ui.cbb_Categories.findText(nouveau_nom))

    def f_supprimer(self):
        """Supprime la catégorie."""
        if self.ui.cbb_Categories.currentIndex() > 0:
            self.f_connect()
            self.cursor.execute("SELECT count(*) FROM table_debiteurs WHERE categorie=?", (self.tableau_cbb_categories[self.categ_selectionnee],))
            for nbr in self.cursor.fetchall():
                if nbr[0] == 0:
                    self.cursor.execute("DELETE FROM table_categories WHERE no_categorie=?", (self.tableau_cbb_categories[self.categ_selectionnee],))
            self.f_disconnect('commit')
            self.f_load_categories()
            self.ui.cbb_Categories.setCurrentIndex(0)

# --------------- SECTION pour tester la GUI indépendamment
def main():
    """Fonction principale pour tester directement le panneau de FenetreGestionCategorie"""
    import sys
    application = QApplication(sys.argv)
    Suivi_Budgetaire_fenetre = FenetreGestionCategorie('BaseDonnees/Analyse_releves_transactions.db')
    Suivi_Budgetaire_fenetre.show()
    sys.exit(application.exec_())

if __name__ == '__main__':
    main()
