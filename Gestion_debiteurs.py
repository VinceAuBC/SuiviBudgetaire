#!/usr/bin/python3
"""Panneau de gestion des débiteurs appellé par le programme Suivi Budgétaire"""
import sqlite3
from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QHeaderView, QApplication
from UI.GestionDebiteurs_ui import Ui_GestionDebiteurs

class FenetreGestionDebiteurs(QMainWindow):
    """Fenetre servant a affecter une catégorie aux débiteurs détectés dans les relevés importés"""
    def __init__(self, nom_bd):
        self.nom_bd = nom_bd
        super(FenetreGestionDebiteurs, self).__init__()

        self.ui = Ui_GestionDebiteurs()
        self.ui.setupUi(self)
        #self.ui.btn_ajout.clicked.connect(self.load_releve)
        self.tableau_tw_debiteur = {}
        self.tableau_cb_categories = {}
        self.f_initialisation()
        self.ui.tw_debiteur.itemSelectionChanged.connect(self.f_selection_debiteur_vers_categ)
        self.ui.cb_categories.currentTextChanged.connect(self.f_selection_categ_vers_debiteur)
        self.ui.pb_AssocierCateg.clicked.connect(self.f_save_categ)
        self.ui.ck_AffDejaAss.stateChanged.connect(self.f_load_tw_debiteur)
        self.ui.pb_Quitter.clicked.connect(self.close)

    def f_connect(self):
        """Connection à la BD"""
        self.connection = sqlite3.connect(self.nom_bd)
        self.cursor = self.connection.cursor()

    def f_disconnect(self, commit):
        """Déconnection de la BD"""
        if commit:
            self.connection.commit()
        self.cursor.close()
        self.connection.close()

    def f_initialisation(self):
        """Initialisation de tout ce qui est 'One-Timer'"""
        self.f_load_tw_debiteur()
        self.f_load_cb_categories()

    def f_selection_debiteur_vers_categ(self):
        """La sélection dans tw_debiteur affecte cb_categories"""
        #--- Juste le premier item de la selection
        #print((self.ui.tw_debiteur.selectedItems())[0].text())
        #--- Tout les items de la selection en list
        #print(list(map(lambda x: x.text(), self.ui.tw_debiteur.selectedItems())))

        #--- Sort uniquement le nom de la categ selectionnée
        if self.ui.tw_debiteur.selectedItems():
            self.ui.pb_AssocierCateg.setEnabled(True)
            nom_selectionne = tuple(map(lambda x: x.text(), self.ui.tw_debiteur.selectedItems()))[-1]
            self.f_connect()
            self.cursor.execute("SELECT c.nom_categorie FROM table_debiteurs d INNER JOIN table_categories c ON d.categorie = c.no_categorie WHERE d.nom_debiteur=?", (nom_selectionne,))
            for nom_categorie in self.cursor.fetchall():
                if nom_categorie[0] != "":
                    self.ui.cb_categories.setCurrentIndex(self.ui.cb_categories.findText(nom_categorie[0]))

            self.f_disconnect('')
        else:
            #self.ui.pb_AssocierCateg.setDisabled(True)
            self.ui.pb_AssocierCateg.setEnabled(False)

    def f_selection_categ_vers_debiteur(self):
        """La sélection dans cb_categories affecte tw_debiteur"""
        # ---- AVEC lambda expression
        #lignes_select = tuple(map(lambda x: x.row(), self.ui.tw_debiteur.selectedItems()))
        #for ligne in lignes_select:
            #self.ui.tw_debiteur.setItem(ligne , 1, QTableWidgetItem(self.ui.cb_categories.currentText()))
        # ----
        for ligne in self.ui.tw_debiteur.selectedItems():
            nom_categorie = self.ui.cb_categories.currentText()
            text_debiteur = self.ui.tw_debiteur.item(ligne.row(), 0).text()
            val_precedente = self.tableau_tw_debiteur[text_debiteur]
            if self.ui.cb_categories.currentText() != "CHOISIR une catégorie":
                self.ui.tw_debiteur.setItem(ligne.row(), 1, QTableWidgetItem(nom_categorie))
                self.tableau_tw_debiteur[text_debiteur] = (1, val_precedente[1], self.tableau_cb_categories[nom_categorie], nom_categorie)
            else:
                self.ui.tw_debiteur.setItem(ligne.row(), 1, QTableWidgetItem(''))
                self.tableau_tw_debiteur[text_debiteur] = (1, val_precedente[1], '', '')

    def f_load_tw_debiteur(self):
        """Ajoute les débiteurs dans le QTabelWidget tw_debiteur"""
        if self.ui.ck_AffDejaAss.isChecked():
            sql_select = '''SELECT d.nom_debiteur, c.nom_categorie, d.no_debiteur, d.categorie
                            FROM table_debiteurs d LEFT OUTER JOIN table_categories c ON d.categorie = c.no_categorie
                            ORDER BY d.nom_debiteur'''
        else:
            sql_select = "SELECT nom_debiteur, NULL, no_debiteur, categorie FROM table_debiteurs WHERE categorie IS NULL ORDER BY nom_debiteur"

        self.f_connect()
        self.cursor.execute(sql_select)
        count = 0
        self.ui.tw_debiteur.clear()
        self.ui.tw_debiteur.setColumnCount(2)
        for nom_debiteur, nom_categorie, no_debiteur, no_categorie in self.cursor.fetchall():
            self.tableau_tw_debiteur[nom_debiteur] = (0, no_debiteur, no_categorie, nom_categorie)
            count += 1
            self.ui.tw_debiteur.setRowCount(count)
            self.ui.tw_debiteur.setItem(count-1, 0, QTableWidgetItem(nom_debiteur))
            self.ui.tw_debiteur.setItem(count-1, 1, QTableWidgetItem(nom_categorie))

        self.f_disconnect('')
        header = self.ui.tw_debiteur.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)

    def f_load_cb_categories(self):
        """Ajoute toutes les catégories dans le cb_categories"""
        self.ui.cb_categories.addItem("CHOISIR une catégorie")
        self.f_connect()
        self.cursor.execute("SELECT nom_categorie, no_categorie FROM table_categories ORDER BY nom_categorie")
        for element in self.cursor.fetchall():
            self.tableau_cb_categories[element[0]] = element[1]
            self.ui.cb_categories.addItem(element[0])

        self.f_disconnect('')

    def f_save_categ(self):
        """Commit à la BD les affectations de catégories effectuées"""
        self.f_connect()
        for debit in self.tableau_tw_debiteur:
            if self.tableau_tw_debiteur[debit][0] == 1:
                self.tableau_tw_debiteur[debit] = (0, self.tableau_tw_debiteur[debit][1], self.tableau_tw_debiteur[debit][2], self.tableau_tw_debiteur[debit][3])
                self.cursor.execute("UPDATE table_debiteurs SET categorie = ? WHERE no_debiteur = ?", (self.tableau_tw_debiteur[debit][2], self.tableau_tw_debiteur[debit][1]))
                #print(debit)
            #else:
                #print("inchangé - {}".format(debit))

        self.f_disconnect('commit')
        self.f_load_tw_debiteur()


def main():
    """Fonction principale pour tester directement le panneau de FenetreGestionDebiteurs"""
    import sys
    application = QApplication(sys.argv)
    gestDeb = FenetreGestionDebiteurs('BaseDonnees/Analyse_releves_transactions.db')
    gestDeb.show()

    sys.exit(application.exec_())

if __name__ == '__main__':
    main()
