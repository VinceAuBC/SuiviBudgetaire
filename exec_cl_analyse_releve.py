#!/usr/bin/python3
"""Class juste pour importer relevé manuellement"""
from os import path
import cl_analyse_releve

def export_all(obj_analyse_releve):
    """Tout exporter les tables 'autres' que transactions"""
    obj_analyse_releve.table_export('table_cc')
    obj_analyse_releve.table_export('table_debiteurs')
    obj_analyse_releve.table_export('table_releves')
    obj_analyse_releve.table_export('table_categories')

def import_all(obj_analyse_releve):
    """Tout importer les tables 'autres' que transactions"""
    obj_analyse_releve.table_import('table_cc')
    obj_analyse_releve.table_import('table_debiteurs')
    obj_analyse_releve.table_import('table_releves')
    obj_analyse_releve.table_import('table_categories')


tst = cl_analyse_releve.cl_analyse_releve('Analyse_releves_transactions.db')

# tst.vider_table('table_categories')
# tst.table_import('table_categories')
# tst.table_export('table_debiteurs')
# import_all(tst)

# Partage réseau
# tst.nom_releve = path.join("..", "Factures cycliques", "Visa Scotia", "VisaScotia_2018-09.csv")
# Répertoire de test
tst.nom_releve = path.join("TEST", "VisaScotia_2018-09.csv")

tst.executer(tst.nom_releve)
tst.table_import('table_categories')
