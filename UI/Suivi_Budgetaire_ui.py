# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Suivi_Budgetaire.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_SuiviBudgetaire(object):
    def setupUi(self, SuiviBudgetaire):
        SuiviBudgetaire.setObjectName("SuiviBudgetaire")
        SuiviBudgetaire.resize(1001, 677)
        self.centralwidget = QtWidgets.QWidget(SuiviBudgetaire)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.tw_transactions = QtWidgets.QTableWidget(self.centralwidget)
        self.tw_transactions.setObjectName("tw_transactions")
        self.tw_transactions.setColumnCount(0)
        self.tw_transactions.setRowCount(0)
        self.gridLayout.addWidget(self.tw_transactions, 3, 0, 1, 6)
        self.btn_refresh = QtWidgets.QPushButton(self.centralwidget)
        self.btn_refresh.setObjectName("btn_refresh")
        self.gridLayout.addWidget(self.btn_refresh, 2, 3, 1, 3)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 4, 3, 1, 1)
        self.btn_quitter = QtWidgets.QPushButton(self.centralwidget)
        self.btn_quitter.setObjectName("btn_quitter")
        self.gridLayout.addWidget(self.btn_quitter, 4, 5, 1, 1)
        self.cbb_date = QtWidgets.QComboBox(self.centralwidget)
        self.cbb_date.setObjectName("cbb_date")
        self.gridLayout.addWidget(self.cbb_date, 2, 0, 1, 3)
        self.lbl_deb_non_ass = QtWidgets.QLabel(self.centralwidget)
        self.lbl_deb_non_ass.setObjectName("lbl_deb_non_ass")
        self.gridLayout.addWidget(self.lbl_deb_non_ass, 1, 0, 1, 3)
        self.lbl_categ_non_ass = QtWidgets.QLabel(self.centralwidget)
        self.lbl_categ_non_ass.setObjectName("lbl_categ_non_ass")
        self.gridLayout.addWidget(self.lbl_categ_non_ass, 1, 3, 1, 3)
        self.btn_importer_rel = QtWidgets.QPushButton(self.centralwidget)
        self.btn_importer_rel.setObjectName("btn_importer_rel")
        self.gridLayout.addWidget(self.btn_importer_rel, 4, 2, 1, 1)
        self.lbl_categories = QtWidgets.QLabel(self.centralwidget)
        self.lbl_categories.setObjectName("lbl_categories")
        self.gridLayout.addWidget(self.lbl_categories, 0, 3, 1, 3)
        self.lbl_debiteurs = QtWidgets.QLabel(self.centralwidget)
        self.lbl_debiteurs.setObjectName("lbl_debiteurs")
        self.gridLayout.addWidget(self.lbl_debiteurs, 0, 0, 1, 3)
        self.btn_categories = QtWidgets.QPushButton(self.centralwidget)
        self.btn_categories.setObjectName("btn_categories")
        self.gridLayout.addWidget(self.btn_categories, 4, 1, 1, 1)
        self.btn_debiteurs = QtWidgets.QPushButton(self.centralwidget)
        self.btn_debiteurs.setObjectName("btn_debiteurs")
        self.gridLayout.addWidget(self.btn_debiteurs, 4, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 4, 4, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        SuiviBudgetaire.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(SuiviBudgetaire)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1001, 32))
        self.menubar.setObjectName("menubar")
        SuiviBudgetaire.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(SuiviBudgetaire)
        self.statusbar.setObjectName("statusbar")
        SuiviBudgetaire.setStatusBar(self.statusbar)

        self.retranslateUi(SuiviBudgetaire)
        QtCore.QMetaObject.connectSlotsByName(SuiviBudgetaire)

    def retranslateUi(self, SuiviBudgetaire):
        _translate = QtCore.QCoreApplication.translate
        SuiviBudgetaire.setWindowTitle(_translate("SuiviBudgetaire", "Suivi Budgétaire"))
        self.btn_refresh.setText(_translate("SuiviBudgetaire", "Rafraîchir"))
        self.btn_quitter.setText(_translate("SuiviBudgetaire", "Quitter"))
        self.lbl_deb_non_ass.setText(_translate("SuiviBudgetaire", "Nombre de débiteurs non assigné :"))
        self.lbl_categ_non_ass.setText(_translate("SuiviBudgetaire", "Nombre de categorie inutilisée : x"))
        self.btn_importer_rel.setText(_translate("SuiviBudgetaire", "Importer un relevé"))
        self.lbl_categories.setText(_translate("SuiviBudgetaire", "Nombre de catégorie : x"))
        self.lbl_debiteurs.setText(_translate("SuiviBudgetaire", "Nombre de débiteurs : x"))
        self.btn_categories.setText(_translate("SuiviBudgetaire", "Catégories"))
        self.btn_debiteurs.setText(_translate("SuiviBudgetaire", "Debiteurs"))

