# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'EmplacementBD.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_EmplacementBD(object):
    def setupUi(self, EmplacementBD):
        EmplacementBD.setObjectName("EmplacementBD")
        EmplacementBD.resize(388, 286)
        self.gridLayout = QtWidgets.QGridLayout(EmplacementBD)
        self.gridLayout.setObjectName("gridLayout")
        self.buttonBox = QtWidgets.QDialogButtonBox(EmplacementBD)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 0, 0, 1, 1)

        self.retranslateUi(EmplacementBD)
        self.buttonBox.accepted.connect(EmplacementBD.accept)
        self.buttonBox.rejected.connect(EmplacementBD.reject)
        QtCore.QMetaObject.connectSlotsByName(EmplacementBD)

    def retranslateUi(self, EmplacementBD):
        _translate = QtCore.QCoreApplication.translate
        EmplacementBD.setWindowTitle(_translate("EmplacementBD", "Emplacement de la BD"))

