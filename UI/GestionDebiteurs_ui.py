# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GestionDebiteurs.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GestionDebiteurs(object):
    def setupUi(self, GestionDebiteurs):
        GestionDebiteurs.setObjectName("GestionDebiteurs")
        GestionDebiteurs.resize(833, 506)
        self.centralwidget = QtWidgets.QWidget(GestionDebiteurs)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pb_AssocierCateg = QtWidgets.QPushButton(self.centralwidget)
        self.pb_AssocierCateg.setEnabled(False)
        self.pb_AssocierCateg.setObjectName("pb_AssocierCateg")
        self.gridLayout.addWidget(self.pb_AssocierCateg, 1, 1, 1, 1)
        self.cb_categories = QtWidgets.QComboBox(self.centralwidget)
        self.cb_categories.setMinimumSize(QtCore.QSize(200, 0))
        self.cb_categories.setObjectName("cb_categories")
        self.gridLayout.addWidget(self.cb_categories, 0, 1, 1, 1)
        self.ck_AffDejaAss = QtWidgets.QCheckBox(self.centralwidget)
        self.ck_AffDejaAss.setObjectName("ck_AffDejaAss")
        self.gridLayout.addWidget(self.ck_AffDejaAss, 2, 1, 1, 1)
        self.tw_debiteur = QtWidgets.QTableWidget(self.centralwidget)
        self.tw_debiteur.setMinimumSize(QtCore.QSize(500, 0))
        self.tw_debiteur.setObjectName("tw_debiteur")
        self.tw_debiteur.setColumnCount(0)
        self.tw_debiteur.setRowCount(0)
        self.gridLayout.addWidget(self.tw_debiteur, 0, 0, 5, 1)
        self.pb_Quitter = QtWidgets.QPushButton(self.centralwidget)
        self.pb_Quitter.setObjectName("pb_Quitter")
        self.gridLayout.addWidget(self.pb_Quitter, 5, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 4, 1, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 3, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        GestionDebiteurs.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(GestionDebiteurs)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 833, 32))
        self.menubar.setObjectName("menubar")
        GestionDebiteurs.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(GestionDebiteurs)
        self.statusbar.setObjectName("statusbar")
        GestionDebiteurs.setStatusBar(self.statusbar)

        self.retranslateUi(GestionDebiteurs)
        QtCore.QMetaObject.connectSlotsByName(GestionDebiteurs)

    def retranslateUi(self, GestionDebiteurs):
        _translate = QtCore.QCoreApplication.translate
        GestionDebiteurs.setWindowTitle(_translate("GestionDebiteurs", "Gestion des débiteurs"))
        self.pb_AssocierCateg.setText(_translate("GestionDebiteurs", "Associer"))
        self.ck_AffDejaAss.setText(_translate("GestionDebiteurs", "Afficher les débteurs déjà\n"
" associés à une catégorie"))
        self.pb_Quitter.setText(_translate("GestionDebiteurs", "Quitter"))
        self.pushButton.setText(_translate("GestionDebiteurs", "PushButton"))

