#!/usr/bin/python3
"""Programme de suivi budgétaire fait pour la famille"""
from os import path
import sqlite3
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QHeaderView, QFileDialog
from PyQt5.QtGui import QFont, QGuiApplication
from UI.Suivi_Budgetaire_ui import Ui_SuiviBudgetaire
from Gestion_debiteurs import FenetreGestionDebiteurs
from Gestion_categorie import FenetreGestionCategorie

class FenetrePrincipale(QMainWindow):
    """Classe pour utiliser le GUI principale et y linker les calls aux autres GUI"""
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        # Set up the user interface from Designer.
        self.ui = Ui_SuiviBudgetaire()
        self.ui.setupUi(self)
        self.nom_bd = path.join(path.dirname(path.realpath(__file__)), "BaseDonnees", "Analyse_releves_transactions.db")
        #self.nom_bd = "BaseDonnees\Analyse_releves_transactions.db"
        self.connection = ""
        self.cursor = ""
        self.liste_releves = {}

        self.fenetres = {}
        self.fenetres['GestDebit'] = FenetreGestionDebiteurs(self.nom_bd)
        self.fenetres['GestCateg'] = FenetreGestionCategorie(self.nom_bd)

        self.f_initialisation()

    def f_connect(self):
        """Connection à la BD"""
        self.connection = sqlite3.connect(self.nom_bd)
        self.cursor = self.connection.cursor()

    def f_disconnect(self, commit):
        """Déconnection de la BD"""
        if commit:
            self.connection.commit()
        self.cursor.close()
        self.connection.close()

    def f_initialisation(self):
        """Initialisation de tout ce qui est 'One-Timer'"""
        self.f_remplir_dates()
        self.ui.btn_quitter.clicked.connect(self.close)
        self.ui.btn_debiteurs.clicked.connect(self.fenetres['GestDebit'].show)
        self.ui.btn_categories.clicked.connect(self.fenetres['GestCateg'].show)
        self.ui.btn_refresh.clicked.connect(self.f_refresh)
        self.ui.cbb_date.currentTextChanged.connect(self.f_remplir_tableau_budget)
        self.ui.btn_importer_rel.setEnabled(True)
        self.ui.btn_importer_rel.clicked.connect(self.f_importer_releve)
        self.ui.btn_importer_rel.setToolTip('Inactif pour le moment... désolé ;)')
        self.f_refresh()

    def f_refresh(self):
        """Rafraîchis les éléments de la page qui peuvent avoir été modifiés par les autres GUI.
        Devra être automatisé par la sortie d'un autre GUI au lieu d'être associé au bouton refresh"""
        self.f_refresh_etiquettes()
        self.f_remplir_tableau_budget()

    def f_refresh_etiquettes(self):
        """Affiche l'information selon le contenu de la BD"""
        lbl_debiteurs = "Nombre de débiteurs : "
        lbl_deb_non_ass = "Nombre de débiteurs non assigné :"
        lbl_categories = "Nombre de catégorie : "
        lbl_categ_non_ass = "Nombre de categorie inutilisée : "
        self.f_connect()
        self.cursor.execute("SELECT count(*) from table_debiteurs")
        self.ui.lbl_debiteurs.setText(lbl_debiteurs + str(tuple(map(lambda x: x[0], self.cursor.fetchall()))[0]))
        self.cursor.execute("SELECT count(*) from table_debiteurs WHERE categorie = '' or categorie IS NULL")
        self.ui.lbl_deb_non_ass.setText(lbl_deb_non_ass + str(tuple(map(lambda x: x[0], self.cursor.fetchall()))[0]))
        self.cursor.execute("SELECT count(*) from table_categories")
        self.ui.lbl_categories.setText(lbl_categories + str(tuple(map(lambda x: x[0], self.cursor.fetchall()))[0]))
        self.cursor.execute("SELECT count(*) from table_categories WHERE no_categorie NOT IN (SELECT DISTINCT categorie FROM table_debiteurs WHERE categorie != '')")
        self.ui.lbl_categ_non_ass.setText(lbl_categ_non_ass + str(tuple(map(lambda x: x[0], self.cursor.fetchall()))[0]))
        self.f_disconnect('')

    def f_remplir_dates(self):
        """Affiche l'information selon le contenu de la BD"""
        self.f_connect()
        # self.cursor.execute('''SELECT date_annee, printf("%02d",date_mois) FROM table_cc_transactions GROUP BY date_annee, date_mois ORDER BY date_annee, date_mois DESC''')
        self.cursor.execute('''SELECT no_releve, nom_releve FROM table_releves''')
        for no_releve, nom_releve in self.cursor.fetchall():
            self.liste_releves[nom_releve] = no_releve
            self.ui.cbb_date.addItem(nom_releve)
        self.f_disconnect('')


    def f_remplir_tableau_budget(self):
        """Rempli le QTableWidget qui contient le résumé du budget selon la date choisie.
        Met automatiquement le contenu du tableau dans le clipboard."""
        no_releve = self.liste_releves[self.ui.cbb_date.currentText()]
        self.f_connect()
        self.cursor.execute('''SELECT Categ AS Catégorie, printf("%.2f",ABS(SUM(ifnull(Somme, 0)))) AS Somme FROM 
                          (SELECT c.nom_categorie AS Categ, t.montant AS Somme FROM  table_categories c
                            LEFT OUTER JOIN table_debiteurs d ON c.no_categorie = d.categorie
                            LEFT OUTER JOIN table_cc_transactions t ON d.no_debiteur = t.debiteur
                            WHERE t.no_releve=? OR Somme IS NULL)
						  GROUP BY Categ''', (no_releve,))
        count = 0
        tableau_donnees = ""
        self.ui.tw_transactions.setColumnCount(2)
        self.ui.tw_transactions.setHorizontalHeaderLabels(("Catégorie", "Somme"))
        montant_font = QFont("Terminal", 10, QFont.Bold)
        for nom_categorie, somme in self.cursor.fetchall():
            count += 1
            tableau_donnees = "{}{}\t{}\n".format(tableau_donnees, nom_categorie, somme)
            self.ui.tw_transactions.setRowCount(count)
            self.ui.tw_transactions.setItem(count-1, 0, QTableWidgetItem(nom_categorie))
            somme_format = QTableWidgetItem("{} $".format(somme))
            somme_format.setTextAlignment(0x0002)
            somme_format.setFont(montant_font)
            self.ui.tw_transactions.setItem(count-1, 1, somme_format)

        self.f_disconnect('')
        header = self.ui.tw_transactions.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)

        QGuiApplication.clipboard().setText(tableau_donnees)

    def f_importer_releve(self):
        self.fenetres['qfd_fic_releve'] = QFileDialog(self, 'Open file', '.',"Relevés .csv (*.csv)")
        self.fenetres['qfd_fic_releve'].setFileMode(1) # 1 = QFileDialog::ExistingFile
        self.fenetres['qfd_fic_releve'].exec_()
        fichier_releve = self.fenetres['qfd_fic_releve'].selectedFiles()
        print(fichier_releve)

def main():
    """Fonction principale pour appeller le panneau de SuiviBudgetaire"""
    import sys
    application = QApplication(sys.argv)
    suivi_budgetaire_fenetre = FenetrePrincipale()
    suivi_budgetaire_fenetre.show()
    sys.exit(application.exec_())

if __name__ == '__main__':
    main()
