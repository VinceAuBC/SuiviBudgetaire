#!/usr/bin/python3
import csv
import sqlite3
from datetime import datetime as dt


class AnalyseReleve(object):
  def __init__(self, *args):
  connection = sqlite3.connect('Analyse_releves_transactions.db')
  c = connection.cursor()

#----------------------------------------------

def create_table():
  c.execute('CREATE TABLE IF NOT EXISTS table_cc(no_carte INTEGER, no_sur_carte INTEGER, expiration REAL, CVV INTEGER, compte INTEGER, info TEXT)')
# no_carte , no_sur_carte, expiration, code endos CVV, institution, compte, info
  c.execute('CREATE TABLE IF NOT EXISTS table_cc_transactions(no_carte INTEGER, date_tr_an INTEGER, date_tr_mois INTEGER, date_tr_jour INTEGER, debiteur TEXT, montant REAL, categorie_forcee INTEGER)')
# no_carte , date_tr_an, date_tr_mois, date_tr_jour, debiteur, montant, categorie_forcee
  c.execute('CREATE TABLE IF NOT EXISTS table_debiteurs(nom_debiteur TEXT, categorie INTEGER, non_constant INTEGER)')
# rowid, nom_debiteur, categorie, non_constant
  c.execute('CREATE TABLE IF NOT EXISTS table_categories(nom_categorie TEXT, note TEXT)')
# no_categorien, nom_categorie, note

#----------------------------------------------

def ajout_transaction(donnee_a_inserer, type_donnee):
  doublon = 0
  if type_donnee == 'transaction':
    sql_Selection = ("SELECT rowid FROM table_cc_transactions WHERE no_carte=? AND date_tr_an=? AND date_tr_mois=? AND date_tr_jour=? AND debiteur=(SELECT rowid FROM table_debiteurs WHERE nom_debiteur=?) AND montant=?")
    sql_Insertion = ("INSERT INTO table_cc_transactions(no_carte, date_tr_an, date_tr_mois, date_tr_jour, debiteur, montant) VALUES (?, ?, ?, ?, (SELECT rowid FROM table_debiteurs WHERE nom_debiteur=?), ?)")
  elif type_donnee == 'debiteur':
    sql_Selection = ("SELECT rowid FROM table_debiteurs WHERE nom_debiteur=?")
    sql_Insertion = ("INSERT INTO table_debiteurs(nom_debiteur) VALUES (?)")

# -------- DEBUG
  # print(type_donnee)
  # print("sql_Selection == {}".format(sql_Selection))
  # print("donnee_a_inserer == {}".format(donnee_a_inserer))
  # print("sql_Insertion == {}".format(sql_Insertion))
# -------- DEBUG

  # c.execute("SELECT * FROM table_debiteurs WHERE nom_debiteur=?", donnee_a_inserer)
  c.execute(sql_Selection, donnee_a_inserer)
  for entree_identique in c.fetchall():
    print("{} déjà présent -> (rowid:{}){}".format(type_donnee, entree_identique, donnee_a_inserer))
    doublon+=1
  if doublon == 0 or type_donnee == 'transaction':
    # c.execute("INSERT INTO table_debiteurs(nom_debiteur) VALUES (?)", donnee_a_inserer)
    c.execute(sql_Insertion, donnee_a_inserer)
    connection.commit()
    return 1
  else:
    doublon = 0
    return 0

#----------------------------------------------
# Les relevés scotia en CSV sont ASCII avec une date M/J/AAAA
# 7/10/2018,"MCDONALD'S #12013  Q04   MONTREAL     QC ",-22.28
def enregistre_releve_scotia():
  count_debiteur=0
  count_transaction=0
  with open('TEST/2018-07_Scotia.csv', 'r', encoding='latin-1') as csvfile:
  # with open('2018-07_Desjardins.csv', 'r', encoding='latin-1') as csvfile:
    ligne_transaction = csv.reader(csvfile, delimiter=',', quotechar='"')
    for date_releve, trans, montant in ligne_transaction:
      an,mois,jour = dt.strptime(date_releve, '%m/%d/%Y').strftime('%Y %m %d').split()
      # print("{} - {} - {}".format(an,mois,jour))
      count_debiteur = count_debiteur + ajout_transaction((trans,), 'debiteur')
      count_transaction = count_transaction + ajout_transaction(('1',an ,mois, jour, trans, montant), 'transaction')
      # print(dt.strptime(date_releve, '%m/%d/%Y').strftime('%Y %m %d'))
  return (count_transaction, count_debiteur)


#----------------------------------------------
# Les relevés scotia en CSV sont Latin-1 avec une date AAAA/MM/JJ
# "Bassin-De-Chambly ","046486","EOP","2018/07/06",00001,"Dépôt - Virement Interac","","",1050.00,"","","","",8950.23
def enregistre_releve_desjardins():
  count_debiteur=0
  count_transaction=0
  with open('TEST/2018-07_Desjardins.csv', 'r', encoding='latin-1') as csvfile:
    ligne_transaction = csv.reader(csvfile, delimiter=',', quotechar='"')
    for nom_caisse, no_compte, type_compte, date_releve, no_operation, nom_transaction, vide1, retrait, depot, vide2, vide3, vide4, vide5, balance in ligne_transaction:
      an,mois,jour = dt.strptime(date_releve, '%Y/%m/%d').strftime('%Y %m %d').split()
      montant = float('0'+depot) - float('0'+retrait)
      count_debiteur = count_debiteur + ajout_transaction((nom_transaction,), 'debiteur')
      count_transaction = count_transaction + ajout_transaction(('1',an ,mois, jour, nom_transaction, montant), 'transaction')
  return (count_transaction, count_debiteur)

def export_debiteurs():
  c.execute("SELECT rowid, nom_debiteur, categorie, non_constant FROM table_debiteurs")
  with open('debiteurs.dump', 'w', newline='', encoding='latin-1') as csvfile:
    ecriture_csv = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    ecriture_csv.writerow(['rowid', 'nom_debiteur', 'categorie', 'non_constant'])
    for rowid, nom_debiteur, categorie, non_constant in c.fetchall():
      ecriture_csv.writerow([rowid, nom_debiteur, categorie, non_constant])

def load_transactions():
  c.execute('SELECT * FROM table_cc_transactions')
  return c.fetchall()


create_table()

nouv_Transactions, nouv_Debiteur = enregistre_releve_scotia()
nouv_Transactions, nouv_Debiteur = enregistre_releve_desjardins()
print("Transactions insérées -> {}\nDébiteurs insérés -> {}".format(nouv_Transactions, nouv_Debiteur))

# export_debiteurs()

c.close()
connection.close()